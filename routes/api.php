<?php

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', 'LoginController@login');
Route::post('/register', 'RegistrationController@store');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/todos/{status?}/{order?}', 'TodoController@index');
    Route::post('/todo', 'TodoController@store');
    Route::put('/update-task', 'TodoController@update');
    Route::get('/users', 'UsersController@index');
    Route::put('/user', 'UsersController@update');
    Route::delete('/user-delete/{id}', 'UsersController@destroy');
    Route::post('/logout', 'UsersController@logout');
});