<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Todo
 * @package App
 */
class Todo extends Model
{
    protected $table = 'todos';

    protected $guarded = [];
}
