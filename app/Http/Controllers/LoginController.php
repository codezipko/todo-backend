<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class LoginController
 * @package App\Http\Controllers
 */
class LoginController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken(env('PASSPORT_APP_KEY'))->accessToken;
                $response = [
                    'token' => $token,
                    'role' => $user->roles
                ];

                return response($response, 200);
            } else {
                $response = "Password missmatch";

                return response($response, 422);
            }
        } else {
            $response = 'User does not exist';

            return response($response, 422);
        }
    }
}
