<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{
    /**
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return User::all();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $authUser = $request->user();
        $user = User::where('id', '=', $request->user_id)->first();

        if ($authUser === 'user') {
            return response('Only admin can edit user table', 422);
        }

        $user->name = $request->name;
        $user->roles = $request->role;
        $user->save();

        return response()->json($user, 201);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function logout(Request $request) {

        $token = $request->user()->token();
        $token->revoke();

        $response = [];

        return response($response, 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $authUser = $request->user();

        if ($authUser->roles === 'user') {
            return response('Only admin can delete user', 422);
        }

        if ($authUser->id === $id) {
            return response('Failed', 500);
        }

        $user = User::find($id);
        $user->delete();

        return response()->json($user, 201);
    }
}
