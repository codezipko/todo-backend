<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

/**
 * Class RegistrationController
 * @package App\Http\Controllers
 */
class RegistrationController extends Controller
{
    public function store(Request $request)
    {
        $user = User::where('email', '=', $request->email)->first();

        if ($user) {
            return response()->json('already_exists', 500);
        }

        $createUser = User::create([
            'name' => $request->username,
            'email' => $request->email,
            'email_verified_at' => now(),
            'password' => Hash::make($request->password),
            'api_token' => '',
            'remember_token' => '',
            'roles' => $request->role,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return response($createUser, 200);
    }
}
