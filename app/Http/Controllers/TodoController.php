<?php

namespace App\Http\Controllers;

use App\Todo;
use App\User;
use Illuminate\Http\Request;

class TodoController extends Controller
{

    public function index(Request $request, $status = 'all', $order = 'asc')
    {
        $authUser = $request->user();

        $tasks = User::find($authUser->id)
            ->tasks()
            ->where(function ($user) use ($status) {
                return ($status !== 'all') ? $user->where('status', $status) : '';
            })
            ->orderBy('created_at', ($order ? $order : 'asc'))
            ->get();

        return $tasks;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('id', '=', $request->user_id)->first();

        if(!$user) {
            return response()->json('', 500);
        }

        $todo = Todo::create([
            'task' => $request->task,
            'user_id' => $user->id,
            'status' => $request->status
        ]);

        return response()->json($todo, 201);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request)
    {
        $authUser = $request->user();
        $todo = Todo::where('id', '=', $request->id)->first();

        if ($todo->user_id !== $authUser->id) {
            return response('You cannot edit this task', 422);
        }

        $todo->status = $request->status;
        $todo->updated_at = new \DateTime();
        $todo->save();

        return response()->json($todo, 201);
    }
}
